﻿// Copyright 2024 Ilya Podtelezhnikov
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "precompiled-relic-editor.hpp"

#include "relic-editor/plugin-system/plugin-manager.hpp"
#include "relic-editor/plugin-system/plugin-instance.hpp"

#include "relic-editor/plugin/plugin.hpp"


namespace RelicEditor::PluginSystem
{
	PluginManager::~PluginManager()
	{
		UnloadAll();
	}

	bool PluginManager::IsLoaded(
		const std::string& id
	) const
	{
		#if defined(_DEBUG)
		assert(!id.empty() && "id is empty");
		#endif

		return _plugins.contains(id);
	}

	std::shared_ptr<Plugin::IPlugin> PluginManager::Load(
		const std::string& filename
	)
	{
		#if defined(_DEBUG)
		assert(!filename.empty() && "filename is empty");
		#endif

		if (filename.empty())
		{
			// todo: write to log
			return nullptr;
		}

		const auto handle = LoadLibraryA(filename.c_str());
		if (!handle)
		{
			// todo: write to log
			return nullptr;
		}

		const auto constructor = reinterpret_cast<Plugin::IPlugin::ConstructorType>(
			GetProcAddress(handle, PluginConstructorFunction.c_str())
		);
		if (!constructor)
		{
			// todo: write to log
			FreeLibrary(handle);
			return nullptr;
		}

		auto plugin = *constructor();
		if (!plugin)
		{
			// todo: write to log
			FreeLibrary(handle);
			return nullptr;
		}

		if (_plugins.contains(plugin->GetId()))
		{
			// todo: write to log
			#if defined(_DEBUG)
			assert(!_plugins.contains(plugin->GetId()) && "Plugin with the same id is already loaded");
			#endif

			FreeLibrary(handle);
			return nullptr;
		}

		_plugins.insert(
			{
				plugin->GetId(),
				PluginInstance {
					.Filename = filename,
					.Plugin = plugin,
					.Constructor = constructor,
					.Handle = handle,
				}
			}
		);

		return plugin;
	}

	bool PluginManager::Unload(
		const std::string& id
	)
	{
		#if defined(_DEBUG)
		assert(!id.empty() && "id is empty");
		#endif

		if (!id.empty())
		{
			// todo: write to log
			return false;
		}

		const auto iterator = _plugins.find(id);
		if (iterator == _plugins.end())
		{
			// todo: write to log
			return false;
		}

		const auto handle = iterator->second.Handle;
		_plugins.erase(iterator);
		return FreeLibrary(handle);
	}

	bool PluginManager::Unload(
		const std::shared_ptr<Plugin::IPlugin>& plugin
	)
	{
		#if defined(_DEBUG)
		assert(!plugin && "plugin is null");
		#endif

		if (!plugin)
		{
			// todo: write to log
			return false;
		}

		return Unload(plugin->GetId());
	}

	std::shared_ptr<Plugin::IPlugin> PluginManager::Reload(
		const std::string& id
	)
	{
		#if defined(_DEBUG)
		assert(!id.empty() && "id is empty");
		#endif

		if (!id.empty())
		{
			// todo: write to log
			return nullptr;
		}

		if (!Unload(id))
		{
			return nullptr;
		}

		return Load(id);
	}

	std::shared_ptr<Plugin::IPlugin> PluginManager::Reload(
		const std::shared_ptr<Plugin::IPlugin>& plugin
	)
	{
		#if defined(_DEBUG)
		assert(!plugin && "plugin is null");
		#endif

		if (!plugin)
		{
			// todo: write to log
			return nullptr;
		}

		return Reload(plugin->GetId());
	}

	std::vector<std::shared_ptr<Plugin::IPlugin>> PluginManager::LoadAll()
	{
		auto plugins = std::vector<std::shared_ptr<Plugin::IPlugin>> {};

		const auto path = std::filesystem::current_path() / PluginDirectory;
		if (!exists(path))
		{
			// todo: write to log
			return {};
		}

		for (const auto& entry : std::filesystem::directory_iterator(path))
		{
			if (!entry.is_regular_file() || entry.path().extension() != PluginExtension)
			{
				continue;
			}

			auto plugin = Load(entry.path().string());
			if (plugin)
			{
				plugins.push_back(plugin);
			}
		}

		return plugins;
	}

	void PluginManager::UnloadAll()
	{
		// todo: check for dependencies

		auto handles = std::vector<HMODULE> {};
		handles.reserve(_plugins.size());

		for (auto& descriptor : _plugins | std::views::values)
		{
			handles.push_back(descriptor.Handle);
		}

		_plugins.clear();

		for (const auto& handle : handles)
		{
			FreeLibrary(handle);
		}
	}

	std::vector<std::shared_ptr<Plugin::IPlugin>> PluginManager::ReloadAll()
	{
		UnloadAll();
		return LoadAll();
	}

	std::shared_ptr<Plugin::IPlugin> PluginManager::GetPlugin(
		const std::string& id
	)
	{
		#if defined(_DEBUG)
		assert(!id.empty() && "id is empty");
		#endif

		const auto iterator = _plugins.find(id);
		return iterator != _plugins.end()
			? iterator->second.Plugin
			: nullptr;
	}

	HMODULE PluginManager::GetHandle(
		const std::string& id
	)
	{
		#if defined(_DEBUG)
		assert(!id.empty() && "id is empty");
		#endif

		const auto iterator = _plugins.find(id);
		return iterator != _plugins.end()
			? iterator->second.Handle
			: nullptr;
	}

	PluginInstance PluginManager::GetDescriptor(
		const std::string& id
	)
	{
		#if defined(_DEBUG)
		assert(!id.empty() && "id is empty");
		#endif

		const auto iterator = _plugins.find(id);
		return iterator != _plugins.end()
			? iterator->second
			: PluginInstance {};
	}

	std::vector<std::string> PluginManager::GetIds()
	{
		auto ids = std::vector<std::string> {};
		ids.reserve(_plugins.size());

		for (const auto& id : _plugins | std::views::keys)
		{
			ids.push_back(id);
		}

		return ids;
	}

	std::vector<std::shared_ptr<Plugin::IPlugin>> PluginManager::GetPlugins()
	{
		auto plugins = std::vector<std::shared_ptr<Plugin::IPlugin>> {};
		plugins.reserve(_plugins.size());

		for (const auto& plugin : _plugins | std::views::values)
		{
			plugins.push_back(plugin.Plugin);
		}

		return plugins;
	}

	std::vector<HMODULE> PluginManager::GetHandles()
	{
		auto handles = std::vector<HMODULE> {};
		handles.reserve(_plugins.size());

		for (const auto& plugin : _plugins | std::views::values)
		{
			handles.push_back(plugin.Handle);
		}

		return handles;
	}

	std::vector<PluginInstance> PluginManager::GetDescriptors()
	{
		auto descriptors = std::vector<PluginInstance> {};
		descriptors.reserve(_plugins.size());

		for (const auto& descriptor : _plugins | std::views::values)
		{
			descriptors.push_back(descriptor);
		}

		return descriptors;
	}

	std::vector<std::shared_ptr<Plugin::IPlugin>> PluginManager::FilterPlugins(
		bool (*filter)(
			std::shared_ptr<Plugin::IPlugin>
		)
	)
	{
		auto plugins = std::vector<std::shared_ptr<Plugin::IPlugin>> {};

		for (const auto& plugin : _plugins | std::views::values)
		{
			if (filter(plugin.Plugin))
			{
				plugins.push_back(plugin.Plugin);
			}
		}

		return plugins;
	}
}
