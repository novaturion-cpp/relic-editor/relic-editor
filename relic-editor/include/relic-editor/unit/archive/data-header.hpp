﻿// Copyright 2024 Ilya Podtelezhnikov
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once
#include <cstdint>


namespace RelicEditor::Unit::Archive
{
	struct DataHeader
	{
		uint32_t Size = 0;
		uint32_t DataOffset = 0;

		uint32_t TablesOffset = 0;
		uint32_t DirectoriesOffset = 0;
		uint32_t FilesOffset = 0;
		uint32_t ItemsOffset = 0;

		/// @brief	Offset to the first table of contents from the start of the file
		uint32_t TablesOffsetAbsolute = 0;
		/// @brief	Offset to the first directory entry from the start of the file
		uint32_t DirectoriesOffsetAbsolute = 0;
		/// @brief	Offset to the first file entry from the start of the file
		uint32_t FilesOffsetAbsolute = 0;
		/// @brief	Offset to the first item entry from the start of the file
		uint32_t ItemsOffsetAbsolute = 0;

		uint16_t TablesCount = 0;
		uint16_t DirectoriesCount = 0;
		uint16_t FilesCount = 0;
		uint16_t ItemsCount = 0;
	};
}
