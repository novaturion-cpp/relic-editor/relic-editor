﻿// Copyright 2024 Ilya Podtelezhnikov
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once
#include <string>


namespace RelicEditor::Unit::Archive
{
	struct TableOfContentsEntry
	{
		std::string Alias = {};
		std::string Name = {};

		uint32_t DirectoryOffset = 0;

		uint16_t DirectoryStart = 0;
		uint16_t DirectoryEnd = 0;
		uint16_t FileStart = 0;
		uint16_t FileEnd = 0;
	};
}
