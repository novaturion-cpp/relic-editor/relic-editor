﻿// Copyright 2024 Ilya Podtelezhnikov
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once
#include <string>


namespace RelicEditor::Unit::Archive
{
	struct DirectoryEntry
	{
		std::filesystem::path Path = {};
		std::string Name = {};

		uint32_t Id = 0;
		uint32_t ParentDirectoryId = 0;

		uint32_t NameOffset = 0;

		uint16_t SubdirectoryIdStart = 0;
		uint16_t SubdirectoryIdEnd = 0;
		uint16_t FileIdStart = 0;
		uint16_t FileIdEnd = 0;
	};
}
