﻿// Copyright 2024 Ilya Podtelezhnikov
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once
#include <memory>
#include <string>

#include "plugin-type.hpp"


namespace RelicEditor::Utility
{
	struct Version;
}


namespace RelicEditor::Plugin
{
	class IPlugin
	{
	public:
		using ConstructorType = std::shared_ptr<IPlugin>*(*)();
		using ConstructorReturnType = std::shared_ptr<IPlugin>*;

	public:
		virtual ~IPlugin() = default;

	public:
		virtual EPluginType GetType() = 0;

		virtual Utility::Version GetVersion() = 0;

		virtual std::string GetId() = 0;

		virtual std::string GetName() = 0;

		virtual std::string GetDescription() = 0;

		// virtual const std::map<std::string, uint32_t>& GetDependencies() = 0;
	};
}
