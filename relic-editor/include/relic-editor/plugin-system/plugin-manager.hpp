﻿// Copyright 2024 Ilya Podtelezhnikov
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

#include "plugin-instance.hpp"

#include "relic-editor/plugin/plugin.hpp"


namespace RelicEditor::PluginSystem
{
	class PluginManager
	{
	public:
		static inline const std::string PluginDirectory = "plugins";
		static inline const std::string PluginExtension = ".dll";
		static inline const std::string PluginConstructorFunction = "ConstructPlugin";

	public:
		PluginManager() = default;

	public:
		~PluginManager();

	public:
		[[nodiscard]]
		bool IsLoaded(
			const std::string& id
		) const;

	public:
		std::shared_ptr<Plugin::IPlugin> Load(
			const std::string& filename
		);

		bool Unload(
			const std::string& id
		);

		bool Unload(
			const std::shared_ptr<Plugin::IPlugin>& plugin
		);

		std::shared_ptr<Plugin::IPlugin> Reload(
			const std::string& id
		);

		std::shared_ptr<Plugin::IPlugin> Reload(
			const std::shared_ptr<Plugin::IPlugin>& plugin
		);

	public:
		std::vector<std::shared_ptr<Plugin::IPlugin>> LoadAll();

		void UnloadAll();

		std::vector<std::shared_ptr<Plugin::IPlugin>> ReloadAll();

	public:
		std::shared_ptr<Plugin::IPlugin> GetPlugin(
			const std::string& id
		);

		HMODULE GetHandle(
			const std::string& id
		);

		PluginInstance GetDescriptor(
			const std::string& id
		);

	public:
		std::vector<std::string> GetIds();

		std::vector<std::shared_ptr<Plugin::IPlugin>> GetPlugins();

		std::vector<HMODULE> GetHandles();

		std::vector<PluginInstance> GetDescriptors();

	public:
		std::vector<std::shared_ptr<Plugin::IPlugin>> FilterPlugins(
			bool (*filter)(
				std::shared_ptr<Plugin::IPlugin>
			)
		);

	private:
		std::unordered_map<std::string, PluginInstance> _plugins = {};
	};
}
