relicEditorIncludeDir = os.getcwd() .. "/include/"

project ( path.getbasename(os.getcwd()) )
    kind "consoleapp"

    pchheader ( precompiledHeader )
    pchsource ( precompiledSource )

    targetname ( binaryName )

    targetdir ( binaryDir )
    objdir ( intermediateDir )

    includedirs { includeDir }
    files { projectFiles, inlineFiles, includeFiles, sourceFiles }
