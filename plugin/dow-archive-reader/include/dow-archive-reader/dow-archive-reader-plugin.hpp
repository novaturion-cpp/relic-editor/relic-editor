﻿// Copyright 2024 Ilya Podtelezhnikov
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once
#include "define.hpp"
#include "relic-editor/plugin/reader-plugin.hpp"


namespace RelicEditor::Unit::Archive
{
	struct Instance;
	struct DataHeader;
	struct DirectoryEntry;
	struct FileEntry;
	struct Header;
	struct TableOfContentsEntry;
}


namespace RelicEditor::Plugin::ArchiveReader
{
	class DowArchiveReaderPlugin : public IReaderPlugin<Unit::Archive::Instance>
	{
	public:
		static std::shared_ptr<IPlugin>& GetInstance();

	public:
		DowArchiveReaderPlugin(
			const DowArchiveReaderPlugin& other
		) = delete;

		DowArchiveReaderPlugin(
			DowArchiveReaderPlugin&& other
		) = delete;

		DowArchiveReaderPlugin& operator=(
			const DowArchiveReaderPlugin& other
		) = delete;

		DowArchiveReaderPlugin& operator=(
			DowArchiveReaderPlugin&& other
		) = delete;

	public:
		virtual ~DowArchiveReaderPlugin() = default;

	public:
		DOW_ARCHIVE_READER_API virtual EPluginType GetType();

		DOW_ARCHIVE_READER_API virtual Utility::Version GetVersion();

		DOW_ARCHIVE_READER_API virtual std::string GetId();

		DOW_ARCHIVE_READER_API virtual std::string GetName();

		DOW_ARCHIVE_READER_API virtual std::string GetDescription();

	public:
		DOW_ARCHIVE_READER_API virtual const std::vector<std::string>& GetSupportedFiles();

	public:
		/// @brief	Read the module file
		/// @throw
		/// @link	RelicEditor.Exception.FileNotFound
		/// @link	RelicEditor.Exception.Generic
		/// @link	RelicEditor.Exception.NotAnArchiveFile
		/// @link	RelicEditor.Exception.UnsupportedFileType
		/// @link	RelicEditor.Exception.UnsupportedFileVersion
		DOW_ARCHIVE_READER_API virtual std::shared_ptr<Unit::Archive::Instance> Read(
			const std::filesystem::path& path
		) noexcept(false);

	public:
		static Unit::Archive::Header ReadHeader(
			std::ifstream& stream
		);

		static Unit::Archive::DataHeader ReadDataHeader(
			std::ifstream& stream
		);

		static Unit::Archive::TableOfContentsEntry ReadTable(
			std::ifstream& stream
		);

		static Unit::Archive::DirectoryEntry ReadDirectory(
			std::ifstream& stream,
			const uint32_t absoluteItemOffset,
			const uint32_t absoluteDirectoryOffset,
			const uint32_t id
		);

		static Unit::Archive::FileEntry ReadFile(
			std::ifstream& stream,
			const uint32_t absoluteItemOffset,
			const uint32_t absoluteFileOffset,
			const uint32_t id
		);

	protected:
		DowArchiveReaderPlugin() = default;

	private:
		static const std::vector<std::string> _supportedFiles;

		static std::shared_ptr<IPlugin> _instance;
	};
}
