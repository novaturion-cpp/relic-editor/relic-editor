dowArchiveReaderIncludeDir = os.getcwd() .. "/include/"

project ( path.getbasename(os.getcwd()) )
    kind "sharedlib"

	links { "relic-editor" }

    pchheader ( precompiledHeader )
    pchsource ( precompiledSource )

    targetname ( binaryName )

    targetdir ( binaryDir )
    objdir ( intermediateDir )

	defines { "DOW_ARCHIVE_READER_EXPORTS" }
    includedirs { includeDir, relicEditorIncludeDir }
    files { projectFiles, inlineFiles, includeFiles, sourceFiles }

    postbuildcommands {
        "{COPYFILE} %[%{cfg.targetdir}/%{prj.name}.dll] %[%{wks.location}/relic-editor/plugins/%{prj.name}.dll]",
        "{COPYFILE} %[%{cfg.targetdir}/%{prj.name}.exp] %[%{wks.location}/relic-editor/plugins/%{prj.name}.exp]",
        "{COPYFILE} %[%{cfg.targetdir}/%{prj.name}.lib] %[%{wks.location}/relic-editor/plugins/%{prj.name}.lib]",
        "{COPYFILE} %[%{cfg.targetdir}/%{prj.name}.pdb] %[%{wks.location}/relic-editor/plugins/%{prj.name}.pdb]",
    }
