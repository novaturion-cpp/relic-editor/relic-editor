// Copyright 2024 Ilya Podtelezhnikov
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "precompiled-dow-archive-reader.hpp"
#include "dow-archive-reader/dow-archive-reader-plugin.hpp"

#include "relic-editor/exception.hpp"
#include "relic-editor/unit/archive/instance.hpp"
#include "relic-editor/utility/version.hpp"


namespace RelicEditor::Plugin::ArchiveReader
{
	const std::vector<std::string> DowArchiveReaderPlugin::_supportedFiles = {".sga"};

	std::shared_ptr<IPlugin> DowArchiveReaderPlugin::_instance = nullptr;


	std::shared_ptr<IPlugin>& DowArchiveReaderPlugin::GetInstance()
	{
		// todo: remove singleton
		if (!_instance)
		{
			_instance.reset(new DowArchiveReaderPlugin());
		}

		return _instance;
	}

	EPluginType DowArchiveReaderPlugin::GetType()
	{
		return EPluginType::UnitReader;
	}

	Utility::Version DowArchiveReaderPlugin::GetVersion()
	{
		return {0, 1, 0, 0};
	}

	std::string DowArchiveReaderPlugin::GetId()
	{
		return "novaturion.dow-archive-reader";
	}

	std::string DowArchiveReaderPlugin::GetName()
	{
		return "DoW Archive Reader";
	}

	std::string DowArchiveReaderPlugin::GetDescription()
	{
		return "Reader of the Dawn of War .sga files";
	}

	const std::vector<std::string>& DowArchiveReaderPlugin::GetSupportedFiles()
	{
		return _supportedFiles;
	}

	std::shared_ptr<Unit::Archive::Instance> DowArchiveReaderPlugin::Read(
		const std::filesystem::path& path
	) noexcept(false)
	{
		if (!exists(path))
		{
			throw Exception::FileNotFound(
				__FILE__,
				__FUNCSIG__,
				"The archive file does not exist.\nPath:\t" + path.string()
			);
		}
		if (path.extension() != _supportedFiles[0])
		{
			throw Exception::UnsupportedFileType(
				__FILE__,
				__FUNCSIG__,
				path.string(),
				path.extension().string(),
				_supportedFiles[0]
			);
		}

		auto stream = std::ifstream(path.c_str(), std::ios::binary);
		if (!stream.is_open())
		{
			throw Exception::Generic(
				__FILE__,
				__FUNCSIG__,
				"Unable to open the archive file.\nPath:\t" + path.string()
			);
		}

		char identifier[9] = {};
		stream.read(identifier, 8);
		if (std::string(identifier) != "_ARCHIVE")
		{
			throw Exception::NotAnArchiveFile(
				__FILE__,
				__FUNCSIG__,
				path.string()
			);
		}

		const auto header = ReadHeader(stream);
		if (header.Version != 2)
		{
			throw Exception::UnsupportedFileVersion(
				__FILE__,
				__FUNCSIG__,
				path.string(),
				std::to_string(header.Version),
				"2"
			);
		}

		auto archive = std::make_shared<Unit::Archive::Instance>();
		archive->Path = path;
		archive->ParentDirectory = path.parent_path();
		archive->Filename = path.filename();

		archive->Header = header;
		archive->DataHeader = ReadDataHeader(stream);

		archive->Tables.reserve(archive->DataHeader.TablesCount);
		for (auto i = 0; i < archive->DataHeader.TablesCount; ++i)
		{
			archive->Tables.push_back(ReadTable(stream));
		}

		archive->Directories.reserve(archive->DataHeader.DirectoriesCount);
		for (auto i = 0u; i < archive->DataHeader.DirectoriesCount; ++i)
		{
			auto directory = ReadDirectory(
				stream,
				archive->DataHeader.ItemsOffsetAbsolute,
				archive->DataHeader.DirectoriesOffsetAbsolute,
				i
			);
			archive->Directories.push_back(directory);
		}

		archive->Files.reserve(archive->DataHeader.FilesCount);
		for (auto i = 0u; i < archive->DataHeader.FilesCount; ++i)
		{
			auto file = ReadFile(
				stream,
				archive->DataHeader.ItemsOffsetAbsolute,
				archive->DataHeader.FilesOffsetAbsolute,
				i
			);
			archive->Files.push_back(file);
		}

		stream.close();

		for (const auto& directory : archive->Directories)
		{
			for (auto i = directory.SubdirectoryIdStart; i < directory.SubdirectoryIdEnd; ++i)
			{
				archive->Directories[i].ParentDirectoryId = directory.Id;
			}
		}

		for (const auto& directory : archive->Directories)
		{
			for (auto i = directory.FileIdStart; i < directory.FileIdEnd; ++i)
			{
				archive->Files[i].ParentDirectoryId = directory.Id;
			}
		}

		return archive;
	}

	Unit::Archive::Header DowArchiveReaderPlugin::ReadHeader(
		std::ifstream& stream
	)
	{
		auto header = Unit::Archive::Header();

		stream.read(reinterpret_cast<char*>(&header.Version), 4);
		stream.read(reinterpret_cast<char*>(header.Md5Tool), 16);

		wchar_t type[65];
		stream.read(reinterpret_cast<char*>(type), 128);
		header.Type = type;

		stream.read(reinterpret_cast<char*>(header.Md5), 16);

		return header;
	}

	Unit::Archive::DataHeader DowArchiveReaderPlugin::ReadDataHeader(
		std::ifstream& stream
	)
	{
		auto dataHeader = Unit::Archive::DataHeader();

		stream.read(reinterpret_cast<char*>(&dataHeader.Size), 4);
		stream.read(reinterpret_cast<char*>(&dataHeader.DataOffset), 4);

		// static_cast<uint32_t>(stream.tellg());
		constexpr auto baseOffset = 180;

		stream.read(reinterpret_cast<char*>(&dataHeader.TablesOffset), 4);
		stream.read(reinterpret_cast<char*>(&dataHeader.TablesCount), 2);
		// subtract 4 bytes for the tables offset and 2 bytes for the tables count
		dataHeader.TablesOffsetAbsolute = baseOffset + dataHeader.TablesOffset;

		stream.read(reinterpret_cast<char*>(&dataHeader.DirectoriesOffset), 4);
		stream.read(reinterpret_cast<char*>(&dataHeader.DirectoriesCount), 2);
		dataHeader.DirectoriesOffsetAbsolute = baseOffset + dataHeader.DirectoriesOffset;

		stream.read(reinterpret_cast<char*>(&dataHeader.FilesOffset), 4);
		stream.read(reinterpret_cast<char*>(&dataHeader.FilesCount), 2);
		dataHeader.FilesOffsetAbsolute = baseOffset + dataHeader.FilesOffset;

		stream.read(reinterpret_cast<char*>(&dataHeader.ItemsOffset), 4);
		stream.read(reinterpret_cast<char*>(&dataHeader.ItemsCount), 2);
		dataHeader.ItemsOffsetAbsolute = baseOffset + dataHeader.ItemsOffset;

		return dataHeader;
	}

	Unit::Archive::TableOfContentsEntry DowArchiveReaderPlugin::ReadTable(
		std::ifstream& stream
	)
	{
		auto table = Unit::Archive::TableOfContentsEntry();

		char buffer[65] = {};
		stream.read(buffer, 64);
		table.Alias = buffer;

		stream.read(buffer, 64);
		table.Name = buffer;

		stream.read(reinterpret_cast<char*>(&table.DirectoryStart), 2);
		stream.read(reinterpret_cast<char*>(&table.DirectoryEnd), 2);

		stream.read(reinterpret_cast<char*>(&table.FileStart), 2);
		stream.read(reinterpret_cast<char*>(&table.FileEnd), 2);

		stream.read(reinterpret_cast<char*>(&table.DirectoryOffset), 4);

		return table;
	}

	Unit::Archive::DirectoryEntry DowArchiveReaderPlugin::ReadDirectory(
		std::ifstream& stream,
		const uint32_t absoluteItemOffset,
		const uint32_t absoluteDirectoryOffset,
		const uint32_t id
	)
	{
		constexpr auto entrySize = 12;
		auto directory = Unit::Archive::DirectoryEntry();

		stream.seekg(absoluteDirectoryOffset + id * entrySize);

		directory.Id = id;
		stream.read(reinterpret_cast<char*>(&directory.NameOffset), 4);
		stream.read(reinterpret_cast<char*>(&directory.SubdirectoryIdStart), 2);
		stream.read(reinterpret_cast<char*>(&directory.SubdirectoryIdEnd), 2);
		stream.read(reinterpret_cast<char*>(&directory.FileIdStart), 2);
		stream.read(reinterpret_cast<char*>(&directory.FileIdEnd), 2);

		stream.seekg(absoluteItemOffset + directory.NameOffset);

		auto byte = '\0';
		do
		{
			stream.read(&byte, 1);
			directory.Path += byte;
		}
		while (byte != '\0');

		directory.Name = directory.Path.filename().string();

		return directory;
	}

	Unit::Archive::FileEntry DowArchiveReaderPlugin::ReadFile(
		std::ifstream& stream,
		const uint32_t absoluteItemOffset,
		const uint32_t absoluteFileOffset,
		const uint32_t id
	)
	{
		constexpr auto entrySize = 20;
		auto file = Unit::Archive::FileEntry();

		stream.seekg(absoluteFileOffset + id * entrySize);

		file.Id = id;
		stream.read(reinterpret_cast<char*>(&file.NameOffset), 4);
		stream.read(reinterpret_cast<char*>(&file.Compression), 4);
		stream.read(reinterpret_cast<char*>(&file.DataOffset), 4);
		stream.read(reinterpret_cast<char*>(&file.DataSize), 4);
		stream.read(reinterpret_cast<char*>(&file.DataSizeCompressed), 4);

		stream.seekg(absoluteItemOffset + file.NameOffset);

		auto byte = '\0';
		do
		{
			stream.read(&byte, 1);
			file.Name += byte;
		}
		while (byte != '\0');

		return file;
	}
}
