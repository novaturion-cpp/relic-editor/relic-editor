﻿// Copyright 2024 Ilya Podtelezhnikov
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once
#include "define.hpp"
#include "relic-editor/plugin/reader-plugin.hpp"


namespace RelicEditor::Unit::Module
{
	struct Instance;
}


namespace RelicEditor::Plugin::ModuleReader
{
	class DowModuleReaderPlugin : public IReaderPlugin<Unit::Module::Instance>
	{
	public:
		static std::shared_ptr<IPlugin>& GetInstance();

	public:
		DowModuleReaderPlugin(
			const DowModuleReaderPlugin& other
		) = delete;

		DowModuleReaderPlugin(
			DowModuleReaderPlugin&& other
		) = delete;

		DowModuleReaderPlugin& operator=(
			const DowModuleReaderPlugin& other
		) = delete;

		DowModuleReaderPlugin& operator=(
			DowModuleReaderPlugin&& other
		) = delete;

	public:
		virtual ~DowModuleReaderPlugin() = default;

	public:
		DOW_MODULE_READER_API virtual EPluginType GetType();

		DOW_MODULE_READER_API virtual Utility::Version GetVersion();

		DOW_MODULE_READER_API virtual std::string GetId();

		DOW_MODULE_READER_API virtual std::string GetName();

		DOW_MODULE_READER_API virtual std::string GetDescription();

	public:
		DOW_MODULE_READER_API virtual const std::vector<std::string>& GetSupportedFiles();

	public:
		/// @brief	Read the module file
		/// @throw
		/// @link	RelicEditor.Exception.FileNotFound
		/// @link	RelicEditor.Exception.Generic
		/// @link	RelicEditor.Exception.MissingIniSection
		/// @link	RelicEditor.Exception.NotAModuleFile
		/// @link	RelicEditor.Exception.OutOfMemory
		/// @link	RelicEditor.Exception.UnsupportedFileType
		/// @link	RelicEditor.Exception.UnsupportedFileVersion
		DOW_MODULE_READER_API virtual std::shared_ptr<Unit::Module::Instance> Read(
			const std::filesystem::path& path
		) noexcept(false);

	public:
		DOW_MODULE_READER_API static std::vector<std::string> ReadLocales(
			const std::filesystem::path& path
		);

	public:
		DOW_MODULE_READER_API static Utility::Version ParseVersion(
			const std::string& value
		);

	protected:
		DowModuleReaderPlugin() = default;

	private:
		static const std::vector<std::string> _supportedFiles;

		static std::shared_ptr<IPlugin> _instance;
	};
}
