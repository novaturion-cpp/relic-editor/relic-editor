﻿// Copyright 2024 Ilya Podtelezhnikov
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "precompiled-dow-module-reader.hpp"
#include "dow-module-reader/dow-module-reader-plugin.hpp"

#include "dow-module-reader/module/dow-instance.hpp"

#include "relic-editor/exception.hpp"


namespace RelicEditor::Plugin::ModuleReader
{
	const std::vector<std::string> DowModuleReaderPlugin::_supportedFiles = {".module"};

	std::shared_ptr<IPlugin> DowModuleReaderPlugin::_instance = nullptr;


	std::shared_ptr<IPlugin>& DowModuleReaderPlugin::GetInstance()
	{
		// todo: remove singleton
		if (!_instance)
		{
			_instance.reset(new DowModuleReaderPlugin());
		}

		return _instance;
	}

	EPluginType DowModuleReaderPlugin::GetType()
	{
		return EPluginType::UnitReader;
	}

	Utility::Version DowModuleReaderPlugin::GetVersion()
	{
		return {0, 1, 0, 0};
	}

	std::string DowModuleReaderPlugin::GetId()
	{
		return "novaturion.dow-module-reader";
	}

	std::string DowModuleReaderPlugin::GetName()
	{
		return "DoW Module Reader";
	}

	std::string DowModuleReaderPlugin::GetDescription()
	{
		return "Reader of Down of War .module files";
	}

	const std::vector<std::string>& DowModuleReaderPlugin::GetSupportedFiles()
	{
		return _supportedFiles;
	}

	std::shared_ptr<Unit::Module::Instance> DowModuleReaderPlugin::Read(
		const std::filesystem::path& path
	) noexcept(false)
	{
		if (!exists(path))
		{
			throw Exception::FileNotFound(
				__FILE__,
				__FUNCSIG__,
				path.string()
			);
		}

		if (path.extension() != _supportedFiles[0])
		{
			throw Exception::UnsupportedFileType(
				__FILE__,
				__FUNCSIG__,
				path.string(),
				path.extension().string(),
				_supportedFiles[0]
			);
		}

		auto iniReader = CSimpleIni(true);

		const auto loadResult = iniReader.LoadFile(path.c_str());
		if (loadResult < 0)
		{
			if (loadResult == -2)
			{
				throw Exception::OutOfMemory(
					__FILE__,
					__FUNCSIG__,
					"Out of memory while reading the module file.\nFile:\t" + path.string()
				);
			}

			throw Exception::Generic(
				__FILE__,
				__FUNCSIG__,
				"Internal error while reading the module file.\n\nError code:\t" + std::to_string(errno) +
				"File:\t" + path.string()
			);
		}

		constexpr auto globalSection = "global";
		if (!iniReader.SectionExists(globalSection))
		{
			throw Exception::MissingIniSection(
				__FILE__,
				__FUNCSIG__,
				path.string(),
				globalSection
			);
		}

		auto sections = CSimpleIni::TNamesDepend();
		iniReader.GetAllSections(sections);
		if (sections.size() > 1)
		{
			throw Exception::UnsupportedFileVersion(
				__FILE__,
				__FUNCSIG__,
				path.string(),
				"Unknown (CoH?)",
				"DoW, DoW: Dark Crusade, DoW: Soulstorm, DoW: Winter Assault"
			);
		}


		if (!(iniReader.KeyExists(globalSection, "ModFolder") && iniReader.KeyExists(globalSection, "ModVersion")))
		{
			throw Exception::NotAModuleFile(
				__FILE__,
				__FUNCSIG__,
				path.string()
			);
		}

		auto module = std::make_shared<Module::DowInstance>();
		module->Path = path;
		module->ParentDirectory = path.parent_path();
		module->ModDirectory = iniReader.GetValue(globalSection, "ModFolder", "");
		module->ModDirectoryAbsolute = module->ParentDirectory / module->ModDirectory;
		module->Filename = std::filesystem::path(path).filename();

		module->UiName = iniReader.GetValue(globalSection, "UIName", "");
		module->Description = iniReader.GetValue(globalSection, "Description", "");
		module->DllName = iniReader.GetValue(globalSection, "DllName", "");
		module->ModVersion = ParseVersion(iniReader.GetValue(globalSection, "ModVersion", ""));
		module->TextureFe = iniReader.GetValue(globalSection, "TextureFE", "");
		module->TextureIcon = iniReader.GetValue(globalSection, "TextureIcon", "");
		module->Playable = iniReader.GetBoolValue(globalSection, "Playable", false);

		module->Locales = ReadLocales(module->ModDirectoryAbsolute);

		auto keys = CSimpleIni::TNamesDepend();
		iniReader.GetAllKeys(globalSection, keys);
		for (const auto& keyEntry : keys)
		{
			const auto key = std::string(keyEntry.pItem);

			if (key.starts_with("DataFolder"))
			{
				module->Directories.emplace_back(
					iniReader.GetValue(globalSection, keyEntry.pItem),
					Unit::Module::EEntryType::Directory,
					std::stoi(key.substr(11))
				);
			}
			else if (key.starts_with("ArchiveFile"))
			{
				module->Archives.emplace_back(
					std::string(iniReader.GetValue(globalSection, keyEntry.pItem)) + ".sga",
					Unit::Module::EEntryType::Archive,
					std::stoi(key.substr(12))
				);
			}
			else if (key.starts_with("RequiredMod"))
			{
				module->Dependencies.emplace_back(
					std::string(iniReader.GetValue(globalSection, keyEntry.pItem)) + ".module",
					Unit::Module::EEntryType::Module,
					std::stoi(key.substr(12))
				);
			}
			else if (key.starts_with("CompatibleMod"))
			{
				module->Compatibles.emplace_back(
					iniReader.GetValue(globalSection, keyEntry.pItem),
					Unit::Module::EEntryType::Compatible,
					std::stoi(key.substr(14))
				);
			}
		}

		return module;
	}

	std::vector<std::string> DowModuleReaderPlugin::ReadLocales(
		const std::filesystem::path& path
	)
	{
		const auto localePath = path / "Locale";
		if (!exists(localePath))
		{
			return {};
		}

		auto locales = std::vector<std::string>();
		for (const auto& entry : std::filesystem::directory_iterator(localePath))
		{
			if (entry.is_regular_file())
			{
				continue;
			}

			locales.emplace_back(entry.path().stem().string());
		}

		return locales;
	}

	Utility::Version DowModuleReaderPlugin::ParseVersion(
		const std::string& value
	)
	{
		auto version = Utility::Version();
		const auto minorPosition = value.find('.');

		if (minorPosition == std::string::npos)
		{
			return version;
		}

		const auto patchPosition = value.find('.', minorPosition + 1);

		version.Major = static_cast<uint8_t>(std::stoi(value.substr(0, minorPosition)));
		version.Minor = static_cast<uint8_t>(
			std::stoi(
				value.substr(
					minorPosition + 1,
					patchPosition != std::string::npos
					? patchPosition
					: value.size()
				)
			)
		);
		version.Patch = patchPosition != std::string::npos
			? static_cast<uint8_t>(std::stoi(value.substr(patchPosition + 1)))
			: 0;

		return version;
	}
}
