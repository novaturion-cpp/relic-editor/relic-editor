simpleIniIncludeDir = os.getcwd() .. "/simple-ini/"

project "simple-ini"
    kind "staticlib"

--     pchheader ( precompiledHeader )
--     pchsource ( precompiledSource )

    targetname ( binaryName )

    targetdir ( thirdPartyBinaryDir )
    objdir ( thirdPartyIntermediateDir )

    includedirs { simpleIniIncludeDir }
    files {
    	thirdPartyProjectFiles,
    	simpleIniIncludeDir .. "simple-ini/*.h",
    	simpleIniIncludeDir .. "simple-ini/*.c"
    }
