dearImguiIncludeDir = os.getcwd() .. "/dear-imgui/"

project "dear-imgui"
    kind "staticlib"

--     pchheader ( precompiledHeader )
--     pchsource ( precompiledSource )

    targetname ( binaryName )

    targetdir ( thirdPartyBinaryDir )
    objdir ( thirdPartyIntermediateDir )

    includedirs { dearImguiIncludeDir .. "dear-imgui/" }
    files {
    	thirdPartyProjectFiles,
    	dearImguiIncludeDir .. "dear-imgui/*.h",
    	dearImguiIncludeDir .. "dear-imgui/*.cpp",
    	dearImguiIncludeDir .. "dear-imgui/**/imgui_stdlib.*",
    	dearImguiIncludeDir .. "dear-imgui/**/*impl_dx12.*",
    	dearImguiIncludeDir .. "dear-imgui/**/*impl_win32.*"
    }
