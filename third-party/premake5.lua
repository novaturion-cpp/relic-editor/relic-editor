thirdPartyDir = os.getcwd() .. "/"

thirdPartyBinaryDir = "binary/%{prj.name}/" .. configuration .. "/"
thirdPartyIntermediateDir = "binary/%{prj.name}/intermediate/" .. configuration .. "/"

include "simple-ini"
include "dear-imgui"
